// init firebase
import firebase from './firebase-integration'

// include vue
import Vue from 'vue'

// include lodash
import lodash from 'lodash'
window._ = lodash

//import VueLodash from 'vue-lodash/dist/vue-lodash.min'
//Vue.use(VueLodash, lodash)

// include vue component library
import Vuetify from 'vuetify'
Vue.use(Vuetify)

// patched vuetify v-dialog
import VuetifyDialogFixed from './components/v-dialog-fixed'
Vue.component('v-dialog-fixed', VuetifyDialogFixed)

// include vue-router
import VueRouter from 'vue-router'
Vue.use(VueRouter)

// include vuefire
import VueFire from './vuefire-custom'
Vue.use(VueFire)

// global components

import MyDataTable from './components/MyDataTable.vue'
Vue.component('my-data-table', MyDataTable)

import BasicDialog from './components/BasicDialog.vue'
Vue.component('basic-dialog', BasicDialog)

// include App, and all the route components
import App          from './App.vue'
import Login        from './Login.vue'
import Dashboard    from './Dashboard.vue'
import ChoreConfig  from './ChoreConfig.vue'
import MemberConfig from './MemberConfig.vue'
import Invites      from './Invites.vue'
import AcceptInvite from './AcceptInvite.vue'
import StartHouse   from './StartHouse.vue'

// configure router
const router = new VueRouter({
	mode: 'history',
	routes: [
		{
			path:      '/', 
			redirect:  to => {
				console.log('redirecting / to /dashboard')
				return '/dashboard'
			},
		},
		{
			path:      '/dashboard', 
			component: Dashboard,
		},
		{
			path:      '/chores',
			component: ChoreConfig,
		},
		{
			path:      '/members',
			component: MemberConfig,
		},
		{
			path:      '/start-a-new-house',
			component: StartHouse,
		},
		{
			name:      'AcceptInvite',
			path:      '/invitation/:inviteHouseId/:inviteMemberId/:inviteId',
			component: AcceptInvite,
			props:     true,
		},
	],
})

// init vue with router config on App
window.root = new Vue({
	router,
	el: '#app',
	render: h => h(App)
})
