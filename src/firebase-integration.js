import firebase from 'firebase'

firebase.initializeApp({
	apiKey:            "AIzaSyCNVs2pMefXoxiHTACSsP9DEb-2kquxZ9Y",
	authDomain:        "chorescore-4aa71.firebaseapp.com",
	databaseURL:       "https://chorescore-4aa71.firebaseio.com",
	projectId:         "chorescore-4aa71",
	storageBucket:     "chorescore-4aa71.appspot.com",
	messagingSenderId: "1039729635386",
})

export default firebase
