import firebase from './firebase-integration'
var db = firebase.database()

export default {
	methods: {
		formatNumber(n) { return Number((n).toFixed(2)) },
		formatDate(d) {
			var date = new Date(d)
			return date.toISOString().substr(0, 10)
		},
		getChoreTitle(choreId) {
			var chore = this.chores[ choreId ]
			return chore.name + ' (' + chore.multiplier + ')'
		},
		retotalScores() {
			try {
				db.ref(`/houses/${this.auth.houseId}/activities`).once('value').then(snapshot => { // should this be a transaction to catch race conditions with activities added between this request and the update below?
					var activities = snapshot.val()
					var memberTotals = _.mapValues(this.members, member => { return 0 })
					_.reduce(activities, (memberTotals, activity) => {
						var memberScoreDeltas = this.calculateMemberScoresForActivity(activity)
						_.each(memberScoreDeltas, (score, memberKey) => memberTotals[memberKey] += score)
						return memberTotals
					}, memberTotals)
					db.ref(`/houses/${this.auth.houseId}/members`).transaction(members => {
						_.each(members, (member, memberId) => {
							member.totalScore = memberTotals[memberId] || 0
						})
						return members
					})
				})
			}
			catch (e) {
				console.error(e)
			}
		},
		calculateMemberScoresForActivity(activity) {
			var memberScoreChanges = _.mapValues(this.choreShareBonuses[ activity.choreId ], bonus => bonus * activity.units)
			memberScoreChanges[ activity.memberId ] += this.chores[activity.choreId].multiplier * activity.units
			return memberScoreChanges
		},
		incrementMemberScores(memberScoreDeltas) {
			db.ref(`/houses/${this.auth.houseId}/members`).transaction(members => {
				_.each(members, (member, memberKey) => {
					member.totalScore += memberScoreDeltas[memberKey] || 0
				})
				return members
			})
		},
	},
	computed: {
		sortedMemberPairs() {
			return _(this.members).toPairs().sortBy('1.order').value()
		},
		sortedChorePairs() {
			return _(this.chores).toPairs().sortBy('1.order').value()
		},
		sortedActivityPairs() {
			return _(this.activities).toPairs().sortBy(([key, { date }]) => { return ((typeof date === 'string') ? (new Date(date).getTime()) : date) }).reverse().value()
		},
		choreShareBonuses() {
			return _.mapValues(this.chores, chore => {

				var memberShares = _.mapValues(this.members, (member, memberKey) => {
					if (chore.shares && chore.shares[ memberKey ] !== undefined) {
						return chore.shares[ memberKey ]
					}
					return 1
				})
				var totalShares     = _(memberShares).values().sum()
				var choreMultiplier = chore.multiplier

				var biggestShare = _(memberShares).values().max()

				return _.mapValues(this.members, (member, memberKey) => {
					var shareBonus = chore.multiplier * (biggestShare - memberShares[ memberKey ]) / totalShares
					shareBonus = Math.round(shareBonus * 100) / 100
					return shareBonus
				})
			})
		},
	},
}
